# wjd-passbolt-backup
`(c) Tobias Hocke 2021`

This script allows to backup a passbolt installation into an Amazon S3 bucket

## Prerequisites
- AWS CLI (check availability with `aws --version`, otherwise install using [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html))
- AWS Account with IAM user which has access to S3 bucket

## Recommended IAM policy
```javascript
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::BUCKET-NAME-HERE/*"
        }
    ]
}
```

## Configure AWS Account
- become root if not `sudo su`
- run `aws configure` and enter credentials

## Install script
```bash
#!/bin/bash

# Passbolt Backup Script
# by Tobias Hocke

# backup steps 
# https://help.passbolt.com/hosting/backup/debian.html

# mysqldump -u[user] -p[pass] [db] > /path/to/backup.sql

PASSBOLT_CONFIG=/etc/passbolt/passbolt.php
PASSBOLT_GPG=/etc/passbolt/gpg

DATABASE_NAME=$(grep -Po -m 1 "(?<='database' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_HOST=$(grep -Po -m 1 "(?<='host' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_PASS=$(grep -Po -m 1 "(?<='password' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_USER=$(grep -Po -m 1 "(?<='username' => ').*(?=',)" $PASSBOLT_CONFIG)

NOW_DAY=$(date +"%Y-%m-%d")
NOW_TIME=$(date +"%Y-%m-%d %T %p")
NOW_MONTH=$(date +"%Y-%m")

BACKUP_TMP="/root/passbolt"
BACKUP_FILENAME="passbolt-$NOW_DAY.tar.gz"
BACKUP_FULLPATH="/root/$BACKUP_FILENAME"

AWS_S3_BUCKET="s3://BUCKET-NAME-HERE/$NOW_MONTH/"

backup_task(){
        mkdir -p ${BACKUP_TMP}
        cp ${PASSBOLT_CONFIG} ${BACKUP_TMP}
        cp -R ${PASSBOLT_GPG} ${BACKUP_TMP}
        mysqldump -u${DATABASE_USER} -p${DATABASE_PASS} ${DATABASE_NAME} > ${BACKUP_TMP}/database.sql
        cd ${BACKUP_TMP}
        tar -zcf ${BACKUP_FULLPATH} *
        aws s3 cp ${BACKUP_FULLPATH} ${AWS_S3_BUCKET}
        rm -R ${BACKUP_TMP}
        rm ${BACKUP_FULLPATH}
}

backup_task

```
- copy the script e.g. to `/root/passbolt-backup.sh`
- give execution permission using `chmod +x /root/passbolt-backup.sh`
- configure variables, at least the bucket name

## Enable cronjob
- enter `crontab -e` to edit the cronjobs
- add `0 1 * * * /root/passbolt-backup.sh > /dev/null 2>&1` for a daily backup on 1am
- save the crontab