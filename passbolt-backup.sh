#!/bin/bash

# Passbolt Backup Script
# by Tobias Hocke

# backup steps 
# https://help.passbolt.com/hosting/backup/debian.html

# mysqldump -u[user] -p[pass] [db] > /path/to/backup.sql

PASSBOLT_CONFIG=/etc/passbolt/passbolt.php
PASSBOLT_GPG=/etc/passbolt/gpg

DATABASE_NAME=$(grep -Po -m 1 "(?<='database' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_HOST=$(grep -Po -m 1 "(?<='host' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_PASS=$(grep -Po -m 1 "(?<='password' => ').*(?=',)" $PASSBOLT_CONFIG)
DATABASE_USER=$(grep -Po -m 1 "(?<='username' => ').*(?=',)" $PASSBOLT_CONFIG)

NOW_DAY=$(date +"%Y-%m-%d")
NOW_TIME=$(date +"%Y-%m-%d %T %p")
NOW_MONTH=$(date +"%Y-%m")

BACKUP_TMP="/root/passbolt"
BACKUP_FILENAME="passbolt-$NOW_DAY.tar.gz"
BACKUP_FULLPATH="/root/$BACKUP_FILENAME"

AWS_S3_BUCKET="s3://BUCKET-NAME-HERE/$NOW_MONTH/"

backup_task(){
        mkdir -p ${BACKUP_TMP}
        cp ${PASSBOLT_CONFIG} ${BACKUP_TMP}
        cp -R ${PASSBOLT_GPG} ${BACKUP_TMP}
        mysqldump -u${DATABASE_USER} -p${DATABASE_PASS} ${DATABASE_NAME} > ${BACKUP_TMP}/database.sql
        cd ${BACKUP_TMP}
        tar -zcf ${BACKUP_FULLPATH} *
        aws s3 cp ${BACKUP_FULLPATH} ${AWS_S3_BUCKET}
        rm -R ${BACKUP_TMP}
        rm ${BACKUP_FULLPATH}
}

backup_task
